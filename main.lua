local Map = require 'tilemap'
local Player = require 'player'

timer = require'timer'

health = 2

local heartOne
local heartTwo

isPlaying = false
local hasStartedOnce = false
local seconds = 0.0

require'newenemy'

local hurt = love.audio.newSource("hurt.wav", "static")
local healthPickup = love.audio.newSource("health.wav", "static")

local logo
local heart
local heartPosition = {x = 0, y = 0}

local isInControls = false

enemies = {}

function love.load()
    love.graphics.setDefaultFilter("nearest")
    logo = love.graphics.newImage("logo.png")
    heart = love.graphics.newImage("heart.png")

    heartOne = love.graphics.newImage("heart.png")
    heartTwo = love.graphics.newImage("heart.png")

    love.graphics.setBackgroundColor(155, 150, 15)
    enemies = {}
    for i = 0, 15 do
        enemies[i] = enemy:create()
    end
    love.graphics.setNewFont(30)
end

function love.update(dt)
    timer.update(dt)
    if isInControls then
    elseif isPlaying then
        seconds = seconds + dt
        updatePlayer()
        for i, e in pairs(enemies) do
            e:update(dt)
            if e.direction ~= -1 then
                if math.floor(e.x) == xPos and math.floor(e.y) == yPos then
                    if health > 0 then
                        health = health - 1
                        love.audio.play(hurt)
                    end
                    e.direction = -1
                elseif math.floor(e.x) == xPos2 and math.floor(e.y) == yPos2 then
                    if health > 0 then
                        health = health - 1
                        love.audio.play(hurt)
                    end
                    e.direction = -1
                end
            end
        end

        if xPos == heartPosition.x and yPos == heartPosition.y then
            if health < 2 then
                health = health + 1
            else
                seconds = seconds + 10
            end
            heartPosition.x = love.math.random(0, 20)
            heartPosition.y = love.math.random(0, 20)
            love.audio.play(healthPickup)
        elseif xPos2 == heartPosition.x and yPos2 == heartPosition.y then
            if health < 2 then
                health = health + 1
            else
                seconds = seconds + 10
            end
            heartPosition.x = love.math.random(0, 20)
            heartPosition.y = love.math.random(0, 20)
            love.audio.play(healthPickup)
        end

        if health <= 0 then
            isPlaying = false
        end
    end
end

function love.draw()
    if isInControls then
        love.graphics.setColor(40, 56, 15)
        
        love.graphics.setNewFont(20)
        love.graphics.print("Survive the white enemy blocks by moving your two blocks around\nwith the arrow keys.\nShift between the two blocks by pressing 'Z'\nYou're stuck together and have to move together.\nWhen the line gets red you have reached the maximum distance.\nSurvive and collect hearts.\nA heart gives 10 points if you're at full health.\nGOOD LUCK!", 20 , love.graphics.getHeight() / 2 - 200)
        love.graphics.setNewFont(30)
        love.graphics.print("PRESS RETURN TO START A NEW GAME", love.graphics.getWidth() / 2 - 280, love.graphics.getHeight() / 2)
    elseif isPlaying then
        drawMap()
        drawPlayer()
        for i, e in pairs(enemies) do
            love.graphics.setColor(200, 200, 40)
            e:draw()
            love.graphics.setColor(255, 0, 0)
        end

        love.graphics.setColor(40, 56, 15)
        love.graphics.print("STAY ALIVE!", love.graphics.getWidth() / 2 - 80, 25)

        love.graphics.setColor(200, 100, 0)

        if health > 0 then
            love.graphics.draw(heartOne, love.graphics.getWidth() / 2 - 20, 60)
        end
        if health > 1 then
            love.graphics.draw(heartTwo, love.graphics.getWidth() / 2, 60)
        end

        love.graphics.draw(heart, 200 + heartPosition.x * 20, 100 + heartPosition.y * 20)

        love.graphics.setColor(40, 56, 15)
        love.graphics.print(round(seconds, 0), love.graphics.getWidth() / 2 - 30, love.graphics.getHeight() - 70)
    else
        love.graphics.setColor(40, 56, 15)
        love.graphics.draw(logo, love.graphics.getWidth() / 2 - 130, love.graphics.getHeight() / 2 - 120, math.rad(0), 8, 8, 16, 16)
        love.graphics.print("PRESS RETURN TO START A NEW GAME\n'C' FOR CONTROLS AND GAME SUMMARY", love.graphics.getWidth() / 2 - 280, love.graphics.getHeight() / 2)
        if hasStartedOnce then
            love.graphics.print("You got "..round(seconds, 0).." points!", love.graphics.getWidth() / 2 - 130, love.graphics.getHeight() / 2 + 65)
        end
    end
end

function love.keypressed( key )
    if isPlaying and isInControls == false then
        keysPressed(key)
    end
    if key == "return" and isPlaying == false or isInControls then
        health = 2
        isPlaying = true
        hasStartedOnce = true
        isInControls = false
        seconds = 0.0
        xPos = 9
        yPos = 10
        xPos2 = 11
        yPos2 = 10
        love.load()
        heartPosition.x = math.random(0, 20)
        heartPosition.y = math.random(0, 20)
    elseif key == "c" and isPlaying == false then
        isInControls = true
    end
end
 
function love.keyreleased( key )
    keysReleased(key)
end

function round(num, numDecimalPlaces)
  local mult = 10^(numDecimalPlaces or 0)
  return math.floor(num * mult + 0.5) / mult
end