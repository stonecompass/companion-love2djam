enemy = 
{ 
    x = -1, y = -1, direction = -1, speed = 5
} -- the table representing the class, which will double as the metatable for the instances

-- syntax equivalent to "MyClass.new = function..."
function enemy:new()
    local self = setmetatable({}, enemy)
    self.x = -1
    self.y = -1
    self.direction = -1
    self.speed = 5
    return self
end

function enemy:create()
  local instance = {}
  setmetatable(instance, {__index = self})
  instance:reset()
  return instance
end

function enemy:reset()
  self.x = -1
  self.y = -1
  self.speed = 3.5
  self.direction = -1
  self:getNewDirection()
end

function enemy:update(dt)
    if self.direction == 0 then --going left
        self.x = self.x - self.speed * dt
    elseif self.direction == 1 then --going up
        self.y = self.y - self.speed * dt
    elseif self.direction == 2 then --going right
        self.x = self.x + self.speed * dt
    elseif self.direction == 3 then --going down
        self.y = self.y + self.speed * dt
    end
end

function enemy:draw()
    if self.direction ~= -1 then
        local flooredX = math.floor(self.x)
        local flooredY = math.floor(self.y)

        if flooredX >= 0 and flooredX <= 20 and flooredY >= 0 and flooredY <= 20 then
            love.graphics.rectangle("fill", 200 + flooredX * 20, 100 + flooredY * 20, 16, 16)
        else
            self.direction = -1
            self:getNewDirection()
        end
    end
end

function enemy:getNewDirection()
    if self.direction == -1 then
        self.direction =  love.math.random(0, 3)
        if self.direction == 0 then --going left
            self.x = 20
            self.y = love.math.random(0, 20)
        elseif self.direction == 1 then --going up
            self.x = love.math.random(0, 20)
            self.y = 20
        elseif self.direction == 2 then --going right
            self.x = 0
            self.y = love.math.random(0, 20)
        elseif self.direction == 3 then --going down
            self.x = love.math.random(0, 29)
            self.y = 0
        end
    end
end